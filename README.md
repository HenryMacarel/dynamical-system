#############################################   
Creator : Macarel Henry  
Date : 11/03/2020   
Programming language : Python3  
Version : 0  
#############################################  

The program's main goal is to study the dynamical system with the following 
equation :

z = (z^2 + p)/(1-q*z^2)
    
z being a random complex number  
p being an input of the dynamical system (complex number)   
q being the conjugate of the complex number p   

This program is separated on a lot of functions that will be detailled there

End